package eingabeAusgabe;

/** Variablen.java
    Ergaenzen Sie nach jedem Kommentar jeweils den Quellcode.
    @author
    @version
*/
public class Variablen {
  public static void main(String [] args){
    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen.
          Vereinbaren Sie eine geeignete Variable */
	 /* int durchlaeufe = xx; */

    /* 2. Weisen Sie dem Zaehler den Wert 25 zu
          und geben Sie ihn auf dem Bildschirm aus.*/
	  
	  int durchlaeufe = 25;
			  System.out.println("durchlaeufe: " + durchlaeufe);
			  

    /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt
          eines Programms ausgewaehlt werden.
          Vereinbaren Sie eine geeignete Variable */ 
	 /* char auswahl_menuepunkt = xx; */

    /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
          und geben Sie ihn auf dem Bildschirm aus.*/
	  char auswahl_menuepunkt = 'C'; 
	  System.out.println("auswahl_menuepunkt: " + auswahl_menuepunkt + "ms");

    /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
          notwendig.
          Vereinbaren Sie eine geeignete Variable */
	 /* double astro_berechnung = xx; */ 
	  
    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
          und geben Sie sie auf dem Bildschirm aus.*/
	  final int LICHTGESCHWINDIGKEIT =  299792458;
	  System.out.println("Lichtgeschwindigkeit: " + LICHTGESCHWINDIGKEIT);

    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
          soll die Anzahl der Mitglieder erfasst werden.
          Vereinbaren Sie eine geeignete Variable und initialisieren sie
          diese sinnvoll.*/
	  short mitglieder = 7;

    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
	  System.out.println("Mitglieder: " + mitglieder);

    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
          Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
          dem Bildschirm aus.*/
	  final double ELEK_ELETARLADUNG = 0.000000000000000000163; /* nicht der genaue Wert */
	  System.out.println("elektrische Elemtarladung: " + ELEK_ELETARLADUNG);

    /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
          Vereinbaren Sie eine geeignete Variable. */
	  
	boolean zahlung_erfolg = true;
		
			 
    /*11. Die Zahlung ist erfolgt.
          Weisen Sie der Variable den entsprechenden Wert zu
          und geben Sie die Variable auf dem Bildschirm aus.*/
	System.out.println(" Zahlung erfolgt: " + zahlung_erfolg);
  }//main
}// Variablen