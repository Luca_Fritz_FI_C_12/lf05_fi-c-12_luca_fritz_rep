﻿import java.util.Scanner;

class Fahrkartenautomat{
    public static void main(String[] args){
    	boolean automat_läuft = true;
    	
    	do {
    	System.out.println("Bitte wählen sie ihre Fahrkarte.");
    	System.out.println("");
    	
    	boolean repeat = true;
    	
    	double gesamtBetrag = fahrkartenbestellungErfassen();
    	
    	
    	double rückgabebetrag = fahrkartenBezahlen(gesamtBetrag);
    	
    	fahrkartenAusgeben();
    	
    	rueckgeldAusgeben(rückgabebetrag);
    	
        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir wünschen Ihnen eine gute Fahrt.");
        System.out.println("");
        warte(2000);
    	} while (automat_läuft == true);
    }
    
    //warte-methode
    public static void warte(long millisekunde) {
    	try {
    		Thread.sleep(millisekunde);
    	}
    	catch(InterruptedException ignored) {
    		
    	}
    }
    
    
    public static double fahrkartenbestellungErfassen() {
    	
    	
    	double[] fahrkarten_preise = {0, 2.90, 3.30, 3.60, 1.90, 8.60, 9.00, 9.60, 23.50, 24.30, 24.90};
    	String[] fahrkarten_namen = {" (0) Abbrechen " , " (1) Einzelfahrschein Berlin AB = ", " (2) Einzelfahrschein Berlin BC = ", " (3) Einzelfahrschein Berlin ABC = ", " (4) Kurzstrecke = ", " (5) Tageskarte Berlin AB = ",
    	 " (6) Tageskarte Berlin BC = ", " (7) Tageskarte Berlin ABC = ", " (8) Kleingruppen-Tageskarte Berlin AB = ", " (9) Kleingruppen-Tageskarte Berlin BC = ", " (10) Kleingruppen-Tageskarte Berlin ABC = "
    	};
    	//int [] fahrkarten_nummern = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
    	
    	Scanner tastatur = new Scanner(System.in);
    	double zuZahlenderBetrag = 0.0; 
    	double ticket_anzahl = 0.0;
    	int ticket_wahl = 0;
        
//        double fahrkarte = tastatur.nextDouble();
        
//        System.out.print("Zu zahlender Betrag (EURO): ");
//        zuZahlenderBetrag = tastatur.nextDouble();
        
//        if (zuZahlenderBetrag < 0) {
//        	System.out.println("Der Angegebene Preis ist negativ.");
//        	System.out.println("Das System hat den Ticketpreis auf 1 Euro gesetzt.");
//        	zuZahlenderBetrag = 1;
//        }
      
    	System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\n \n");
    	
    	
    	for (int i = 0; i < fahrkarten_namen.length; i++) {
    		
    		System.out.println(fahrkarten_namen[i] + " " + fahrkarten_preise[i]);
    	}
    	
//    			+" Einzelfahrschein Berlin AB (1)\n \n"
//    			+" Einzelfahrschein Berlin BC(2)\n \n"
//    			+" Einzelfahrschein Berlin ABC(3)\n \n"
//    			+" Kurzstrecke(4)\n \n"
//    			+" Tageskarte Berlin AB (5)\n \n"
//    			+" Tageskarte Berlin BC (6)\n \n"
//    			+" Tageskarte Berlin ABC (7)\n \n"
//    			+" Kleingruppen-Tageskarte Berlin AB (8)\n \n"
//    			+" Kleingruppen-Tageskarte Berlin BC (9)\n \n"
//    			+" Kleingruppen-Tageskarte Berlin ABC (10)\n \n");
    			
    	ticket_wahl = tastatur.nextInt();
    	

    	
    	zuZahlenderBetrag = (fahrkarten_preise[ticket_wahl]);
    	
    	
//    	Wird nicht mehr gebraucht, wegen den Arrays 
//    	
//    	if (ticket_wahl == 1) {
//    		zuZahlenderBetrag = 2.90;
//    	}
//    	if (ticket_wahl == 2) {
//    		zuZahlenderBetrag = 8.60;
//    	}
//    	if (ticket_wahl == 3) {
//    		zuZahlenderBetrag = 23.50;
//    	}
    	
        System.out.print("Wie viele Tickets: ");
        ticket_anzahl = tastatur.nextDouble();
        
        if (ticket_anzahl <= 0) {
        	System.out.println("Sie haben leider zu wenige Tickets angegeben (min.1).");
        	System.out.println("Das System hat die Ticket Anzahl auf 1 Ticket gsetzt.");
        	ticket_anzahl = 1;
        }
        if (ticket_anzahl > 10) {
        	System.out.println("Sie haben leider zu viele Tickets angegeben (max. 10).");
        	System.out.println("Das System hat die Ticket Anzahl auf 1 Ticket gsetzt.");
        	ticket_anzahl = 1;
        }
        double gesamtBetrag = zuZahlenderBetrag * ticket_anzahl;
      return gesamtBetrag;
    	}
    
     
    
    public static double fahrkartenBezahlen(double gesamtBetrag) {
 	   
 	   Scanner tastatur = new Scanner(System.in);
 	   
 	   
 	   double eingezahlterGesamtbetrag = 0.0;
	   double eingeworfeneMünze;
	  
	   
//	   System.out.print("Zu zahlender Betrag (Euro): ");
//	   eingezahlterGesamtbetrag = tastatur.nextDouble();
//	       
	   
	   
//	   System.out.println("");
//	   
//	   System.out.printf("Noch zu zahlen: %.2f EURO \n" , (gesamtBetrag - eingezahlterGesamtbetrag));
//	   eingeworfeneMünze = tastatur.nextDouble();
//	   gesamtBetrag -= eingeworfeneMünze;
	   
	   
	 
	   while(eingezahlterGesamtbetrag < gesamtBetrag)
	   {
		   System.out.println("");
		   System.out.printf("Noch zu zahlen: %.2f EURO \n" , (gesamtBetrag - eingezahlterGesamtbetrag ));
		   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
		   eingeworfeneMünze = tastatur.nextDouble();
		   eingezahlterGesamtbetrag += eingeworfeneMünze;
		   
//		   System.out.println(eingezahlterGesamtbetrag);
//		   System.out.println(gesamtBetrag);
		   
		   
//		   if (eingezahlterGesamtbetrag < gesamtBetrag) {
//			   System.out.print("While Erfüllt");
//		   }
		   
	   }
	   
	      return eingezahlterGesamtbetrag - gesamtBetrag;
    }
    
    
//    Fahrkarten Ausgabe
    
    public static void fahrkartenAusgeben() {
    	
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		} catch (InterruptedException e) {
 			// TODO Auto-generated catch block
 			e.printStackTrace();
 		}
        }
        System.out.println("\n\n");
    }
 
    
    
//    Muenz Ausgabe
    
    public static double  rueckgeldAusgeben (double rückgabebetrag) {
    	

        if(rückgabebetrag >= 0.0){
        	 System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro wird in folgenden Münzen ausgezahlt: \n", rückgabebetrag);
        	 
     	   rückgabebetrag = Math.round(rückgabebetrag*100)/100.0;

            while(rückgabebetrag >= 2.0) // 2 EURO-Münzen 2.0
            {
         	  rückgabebetrag = Math.round(rückgabebetrag*100)/100.0;
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.0;	
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen 1.0
            {
         	  rückgabebetrag = Math.round(rückgabebetrag*100)/100.0;
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.0;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  rückgabebetrag = Math.round(rückgabebetrag*100)/100.0;
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.5;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  rückgabebetrag = Math.round(rückgabebetrag*100)/100.0;
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.2;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  rückgabebetrag = Math.round(rückgabebetrag*100)/100.0;
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.1;
            }
            while(rückgabebetrag >= 0.049)// 5 CENT-Münzen
            {
         	  rückgabebetrag = Math.round(rückgabebetrag*100)/100.0;
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05;
            }
        }     
        return rückgabebetrag;
   
    }
}

