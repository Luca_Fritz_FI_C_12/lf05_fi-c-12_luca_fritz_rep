/**
 * 
 * @author luca.fritz
 * @since 18.05.2002
 * <p>
 * Dies ist die Ladungsklasse des Raumschiffs 
 */
public class Ladung {
	
	private String bezeichnung;
	private int menge;

	public Ladung() {
		System.out.println("Ladung-objekt");

	}
	
	/**
	 * 
	 * @param bezeichnung
	 * @param menge
	 * <p>
	 * Definiert die Parameter der Ladung.
	 */
	public Ladung(String bezeichnung, int menge) {
		this.bezeichnung = bezeichnung;
		this.menge = menge; 
	}
	
	/**
	 * 
	 * @return String
	 * <p>
	 * Gibt einen die Bezeichnung der Ladungsgegenstaende wieder.
	 */
	public String getBezeichnung() {
		return bezeichnung;
	}
	
	/**
	 * 
	 * @param bezeichnung
	 * <p>
	 * Setzt die Bezeichnung f�r Ladungsgegenstaende.
	 */
	public void setBezeichnung(String bezeichnung) {
		this.bezeichnung = bezeichnung;
	}
	
	/**
	 * 
	 * @return int
	 * <p>
	 * Gibt die Menge der einzelnen Ladungen an.
	 */
	public int getMenge() {
		return menge;
	}
	
	/**
	 * 
	 * @param menge
	 * <p>
	 * Setzt die Menge einer bestimmten Ladung.
	 */
	public void setMenge(int menge) {
		this.menge = menge;
	}
	
	/**
	 * Dies ist die toString Methode
	 * Erlaubt es uns die Ladung auszugeben.
	 */
	@Override
	public String toString() {
		return bezeichnung + "("+menge+")";
	}
	
	
}
