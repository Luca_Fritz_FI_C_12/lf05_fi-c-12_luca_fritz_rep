
public class �bung2 {

	public static void main(String[] args) {
	
	// Aufgabe 1
		
	System.out.println(" Aufgabe 1: \n ");
		
	System.out.printf("%10s %n", "**" );
	System.out.printf("%6s","*");
	System.out.printf("%7s %n", "*" );
	System.out.printf("%6s","*");
	System.out.printf("%7s %n", "*" );
	System.out.printf("%10s %n", "**" );

	// Aufgabe 2
	
	System.out.println("\n Aufgabe 2: \n ");
	
	System.out.printf("%-5s=%-19s=%4s %n", "0!", "", "1");
	System.out.printf("%-5s=%-19s=%4s %n", "1!", "1", "1");
	System.out.printf("%-5s=%-19s=%4s %n", "2!", "1 * 2", "2");
	System.out.printf("%-5s=%-19s=%4s %n", "3!", "1 * 2 * 3", "6");
	System.out.printf("%-5s=%-19s=%4s %n", "4!", "1 * 2 * 3 * 4", "24");
	System.out.printf("%-5s=%-19s=%4s %n", "5!", "1 * 2 * 3 * 4 * 5", "120");
	
	// Aufgabe 3
	
	System.out.println("\n Aufgabe 3: \n ");
	
	System.out.printf("%-12s|%10s %n", "Fahrenheit", "Celsius");
	System.out.print("------------------------- \n");
	System.out.printf("%-12s|%10.6s %n", "-20", "-28.8889");
	System.out.printf("%-12s|%10.6s %n", "-10", "-23.3333");
	System.out.printf("%-12s|%10.6s %n", "0  ", "-17.7778");
	System.out.printf("%-12s|%10.5s %n", "20 ", "-6.6667");
	System.out.printf("%-12s|%10.5s %n", "30 ", "-1.1111");
	
	}
	
	
	
}
