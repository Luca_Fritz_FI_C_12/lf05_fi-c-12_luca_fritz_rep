import java.util.Scanner;

public class SteuerMethode {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner scn = new Scanner(System.in);

		Raumschiff klingonen = new Raumschiff(1, 100, 100, 100, 100, "IKS Hegh�ta", 2);

		Raumschiff romulaner = new Raumschiff(2, 100, 100, 100, 100, "IRW Khazara", 2);

		Raumschiff vulkanier = new Raumschiff(0, 80, 80, 50, 100, "Ni�Var", 5);

		// Ladung klingonen

		Ladung NeueLadung1 = new Ladung("Ferengi Schneckensaft", 200);
		klingonen.addLadung(NeueLadung1);

		Ladung NeueLadung2 = new Ladung("Bat`leth Klingonen Schwert", 200);
		klingonen.addLadung(NeueLadung2);

		// Ladung romulaner

		Ladung NeueLadung3 = new Ladung("Borg-Schrott", 5);
		romulaner.addLadung(NeueLadung3);

		Ladung NeueLadung4 = new Ladung("Rote Materie", 2);
		romulaner.addLadung(NeueLadung4);

		Ladung NeueLadung5 = new Ladung("Plasma-Waffe", 50);
		romulaner.addLadung(NeueLadung5);

		// Ladung vulkanier

		Ladung NeueLadung6 = new Ladung("Forschungssonde", 35);
		vulkanier.addLadung(NeueLadung6);

		Ladung NeueLadung7 = new Ladung("Photonentorpedo", 3);
		vulkanier.addLadung(NeueLadung7);

		// Raumschiff Ausgabe 
		
		
		// Raumschiff der klingonen
		System.out.println("");
		/*System.out.println("Schiffsname: " + klingonen.getSchiffsname());
		System.out.println("EnergieversorgungInProzent: " + klingonen.getEnergieversorgungInProzent());
		System.out.println("Ladungsverzeichnis: " + klingonen.getLadungsverzeichnis());
		System.out.println("SchildeInProzent: " + klingonen.getSchildeInProzent());
		System.out.println("HuelleInProzent: " + klingonen.getHuelleInProzent());
		System.out.println("LebenserhaltungssystemeInProzent: " + klingonen.getLebenserhaltungssystemeInProzent());
		System.out.println("AndroidenAnzahl: " + klingonen.getAndroidenAnzahl());*/
		//klingonen.zustandRaumschiff();
		
		
		//Raumschiff der romulaner
		
		//romulaner.zustandRaumschiff();
		
		// Raumschiff der vulkanier
		
		//vulkanier.zustandRaumschiff();
		
		
		
		
		klingonen.photonentorpedoSchiessen(romulaner);
	
		romulaner.phaserkanoneSchiessen(romulaner);
		System.out.println("");
		
		System.out.print(vulkanier.getSchiffsname() + ": ");
		vulkanier.NachrichtAnAlle("Gewalt ist nicht logisch.");
		System.out.println("");
		
		klingonen.zustandRaumschiff();
		System.out.println("");
		
		klingonen.photonentorpedoSchiessen(romulaner);
		klingonen.photonentorpedoSchiessen(romulaner);
		System.out.println("");
		
		romulaner.zustandRaumschiff();
		vulkanier.zustandRaumschiff();
		
		vulkanier.eintraegeLogbuchZurueckgeben();
		romulaner.eintraegeLogbuchZurueckgeben();
		klingonen.eintraegeLogbuchZurueckgeben();
		
		
	}

}
