import java.util.Scanner;

public class Kontrollstrukturen {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner scn = new Scanner(System.in);
		System.out.println("Erste Zahl eingeben: ");
		int zahl01 = scn.nextInt();
		System.out.println("Zweite Zahl eingeben: ");
		int zahl02 = scn.nextInt();
		
		// Wenn beide Zahlen gleich sind, Nachricht in der Konsole. 
		// if Einleitung 
		
		if (zahl01 == zahl02) {
		
			System.out.println("Beide Zahlen sind gleich!\n");
		}
		else {
			System.out.println("Beide Zahlen sind verschieden!\n");
		}
		
		if (zahl01 >= zahl02) {
			
			System.out.println("Zahl 1 ist gr��er / gleich als Zahl 2!\n");
		}
		else {
			System.out.println("Zahl 1 ist kleiner / gleich als Zahl 2!\n");
		}
		
		if (zahl01 > zahl02) {
			
			System.out.println("Zahl 1 ist gr��er als Zahl 2!\n");
		}
		else {
			System.out.println("Zahl 1 ist kleiner gleich Zahl 2!\n");
			
		}
		
		
		
	}
	
	

}
