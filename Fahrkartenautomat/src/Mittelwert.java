import java.util.Scanner;

public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
//      double x = 2.0;
//      double y = 4.0;
//      double m;
      
    
      // (V) Verarbeitung
      // Mittelwert von x und y berechnen: 
      // ================================
      //m = (x + y) / 2.0;
      
      // (A) Ausgabe
      // Ergebnis auf der Konsole ausgeben:
      // =================================
      //System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
      
      int x = Eingabe("Erste Zahl eingeben");
      int y = Eingabe("Zweite Zahl eingeben");
      int m = Verarbeitung(x, y);
      System.out.printf("Der Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
    		  
   }
   
//   public static void berechneMittelwert(int ersteZahl, int zweiteZahl) {
//
//	   System.out.print((ersteZahl+ zweiteZahl) / 2.0);
//	  
//	  
//   }
   
   public static int Verarbeitung(int x, int y) {
	   
	  int m = (x + y) / 2;
	  return m;
   }
   public static int Eingabe(String text) {
	   
	   Scanner scn = new Scanner(System.in);
	   System.out.print(text);
	   int x = scn.nextInt();
	   return x;
   }
}
