import java.util.Scanner;

public class Schleifen {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner input = new Scanner(System.in);
		System.out.print("Nummer eingeben: ");
		int nummer = input.nextInt();
		
		int zaehler = 1;
		
		while(zaehler <= nummer) {
			System.out.println(zaehler);
			zaehler ++;
			
		}
		System.out.println("\nfor-loop\n");
		
		for(int i = 1; i <= nummer; i++) {
			System.out.println(i);
			
		}
	}

}
