import java.util.Scanner;

public class Aufgabe_2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 Scanner myScanner = new Scanner(System.in);
		 
		System.out.println("Guten Tag, wie alt bist du? ");
		int alter = myScanner.nextInt();
		
		System.out.println("Wie ist dein Name? ");
		String name = myScanner.next();
		
		System.out.println("\n Dein Name ist: " + name + "\n Du bist " + alter + " Jahre alt.");
	}

}
