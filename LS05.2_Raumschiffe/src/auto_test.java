import java.util.Scanner;

public class auto_test {

	public static void main(String[] args) {
		
		Scanner scn = new Scanner(System.in);
		
		Auto a = new Auto();
		
		Auto b = new Auto("fiat", "rot");
		
		b.setMarke("Honda");
		System.out.println(b.getMarke());
		
		b.setFarbe("Blau");
		System.out.println(b.getFarbe());
		
		b.fahre(12);
		
		b.tanke(25);
	}

}
