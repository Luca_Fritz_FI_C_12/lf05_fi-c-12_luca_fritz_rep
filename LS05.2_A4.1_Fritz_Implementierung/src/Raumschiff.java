import java.util.ArrayList;
/**
 * 
 * @author Luca.Fritz
 * @since 18.05.2022
 * <p>
 * Diese Klasse erstellt und bestimmt die Parameter der Klasse Raumschiff  (@ = Keywords)
 */
public class Raumschiff {

	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;

	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	private ArrayList<String> broadcastKommunikator = new ArrayList<String>();

	public Raumschiff() {
		System.out.println("Ladung-objekt");

	}
	/**
	 * 
	 * @param photonentorpedoAnzahl
	 * @param energieversorgungInProzent
	 * @param schildeInProzent
	 * @param huelleInProzent
	 * @param lebenserhaltungssystemeInProzent
	 * @param schiffsname
	 * @param androidenAnzahl
	 * <p>
	 * Hier wird der Aufbau eines Raumschiffes deklariert.
	 */
	public Raumschiff(int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent,
			int huelleInProzent, int lebenserhaltungssystemeInProzent, String schiffsname, int androidenAnzahl) {

		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}
	
	/**
	 * 
	 * @return String
	 * <p>
	 * Gibt die Anzahl der Photonentorpedos wieder.
	 */
	public int getPhotonentorpedoAnzahl() {
		return photonentorpedoAnzahl;
	}
	
	/**
	 * 
	 * @param photonentorpedoAnzahl
	 * <p>
	 * Setzt die Anzahl der Photonentorpedos
	 */
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}
	
	/**
	 * 
	 * @return int
	 * <p>
	 * Gibt die Prozentmenge der Engergieversogung.
	 */
	public int getEnergieversorgungInProzent() {
		return energieversorgungInProzent;
	}
	
	/**
	 * 
	 * @param energieversorgungInProzent
	 * <p>
	 * Setzt die prozentuale Menge der Energieversogung.
	 */
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}
	
	/**
	 * 
	 * @return int 
	 * <p>
	 * Gibt die Haltbarkeit der Schilde in Prozent wieder.
	 */
	public int getSchildeInProzent() {
		return schildeInProzent;
	}
	
	/**
	 * 
	 * @param schildeInProzent
	 * <p>
	 * Setzt die Haltbarkeit in % der Schilde.
	 */
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}
	
	/**
	 * 
	 * @return int 
	 * <p>
	 * Gibt die Haltbarkeit der Huelle in % wieder.
	 */
	public int getHuelleInProzent() {
		return huelleInProzent;
	}
	
	/**
	 * 
	 * @param huelleInProzent
	 * <p>
	 * Setzt die Haltbarkeit der Huelle in %.
	 */
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}
	
	/**
	 * 
	 * @return int
	 * <p>
	 * Gibt die Lebenserhaltungssysteme in % wieder.
	 */
	public int getLebenserhaltungssystemeInProzent() {
		return lebenserhaltungssystemeInProzent;
	}
	
	/**
	 * 
	 * @param lebenserhaltungssystemeInProzent
	 * Setzt die Lebenserhaltungssyteme in %.
	 */
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}
	
	/**
	 * 
	 * @return int
	 * Gibt die Anzahl der Androiden wieder.
	 */
	public int getAndroidenAnzahl() {
		return androidenAnzahl;
	}

	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
	
	/**
	 * 
	 * @return String
	 * <p>
	 * Gibt den Schiffsnamen wieder.
	 */
	public String getSchiffsname() {
		return schiffsname;
	}
	
	/**
	 * 
	 * @param schiffsname
	 * <p>
	 * Setzt den Schiffsnamen.
	 */
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}
	
	/**
	 * 
	 * @return ArrayList
	 * <p>
	 * Gibt das Ladungsverzeichnis wieder.
	 */
	public ArrayList<Ladung> getLadungsverzeichnis() {
		return ladungsverzeichnis;
	}
	
	/**
	 * 
	 * @param ladungsverzeichnis
	 * <p>
	 * Fuegt Ladung dem Ladungsverzeichnis hinzu. 
	 */
	public void setLadungsverzeichnis(ArrayList<Ladung> ladungsverzeichnis) {
		this.ladungsverzeichnis = ladungsverzeichnis;
	}

	// weitere Methoden
	
	/**
	 * 
	 * @param ladung
	 * <p>
	 * Fueght ein defniertes Objekt der Ladung hinzu.
	 */
	public void addLadung(Ladung ladung) {
		this.ladungsverzeichnis.add(ladung);
	}
	/**
	 * 
	 * @param ziel
	 * <p>
	 * Gibt wieder ob geschossen werden kann und setzt das getroffene Zie
	 */
	public void photonentorpedoSchiessen(Raumschiff ziel) {

		if (photonentorpedoAnzahl == 0) {
			NachrichtAnAlle("-=*Click*=-");
		}

		if (photonentorpedoAnzahl > 0) {
			photonentorpedoAnzahl = photonentorpedoAnzahl - 1;
			NachrichtAnAlle(schiffsname + " hat einen Photonentorpedo auf " + ziel.schiffsname + " geschossen");

			treffer(ziel);
		}

	}
	
	/**
	 * 
	 * @param ziel
	 * <p>
	 * Gibt wieder ob geschossen werden kann und setzt das getroffene Ziel.
	 */
	public void phaserkanoneSchiessen(Raumschiff ziel) {
		if (energieversorgungInProzent < 50) {
			NachrichtAnAlle(schiffsname + "-=*Click*=-");
		}

		if (energieversorgungInProzent >= 50) {
			energieversorgungInProzent = energieversorgungInProzent - 50;
			NachrichtAnAlle(schiffsname + " hat die Phaserkanone auf " + ziel.schiffsname + " geschossen");
			treffer(ziel);
		}

	}
	
	/**
	 * 
	 * @param message
	 * <p>
	 * Sendet eine Nachricht an alle.
	 */
	public void NachrichtAnAlle(String message) {
		System.out.println(message);
		addLogbuchEintrag(message);
	}
	
	/**
	 * 
	 * @param ziel
	 * <p>
	 * Fuehrt die Schadenberechnung des Ziels durch.
	 */
	private void treffer(Raumschiff ziel) {
		System.out.println(ziel.schiffsname + " wurde getroffen");

		if (ziel.schildeInProzent > 50) {
			ziel.schildeInProzent = schildeInProzent - 50;
		}

		else if (ziel.schildeInProzent <= 50) {
			ziel.schildeInProzent = 0;
			if (ziel.huelleInProzent > 50) {

				ziel.huelleInProzent = huelleInProzent - 50;
				ziel.energieversorgungInProzent = energieversorgungInProzent - 50;
			} else {
				ziel.huelleInProzent = 0;
				ziel.energieversorgungInProzent = 0;
				System.out.println("Die Lebenserhaltungssystem wurden zerst�rt.");
				ziel.lebenserhaltungssystemeInProzent = 0;
			}
			// Schilde werden von einem beliebigen Wert unter 50 auf 0 gesetzt.
		}
	}
	
	/**
	 * public void eintraegeLogbuchZurueckgeben()
	 * <p>
	 * Gibt das Logbuch wieder.
	 */
	public void eintraegeLogbuchZurueckgeben() {
		System.out.println(schiffsname + " Logbuch " + broadcastKommunikator);
	}

	/**
	 * 
	 * @param broadcastKommunikator
	 * <p>
	 * Fuegt einen brodcast dem Logbuch hinzu.
	 */
	public void addLogbuchEintrag(String broadcastKommunikator){
		this.broadcastKommunikator.add(broadcastKommunikator);
	}
	
	
	
	// Ladung NeueLadung = new Ladung("Kiste", 10);
	// in der Kontroll Methode: b.addLadung(NeueLadung);
	
	/**
	 * 
	 * @param anzahlTorpedos
	 * <p>
	 * Laedt neue photonentorpedos.
	 */
	public void photonentorpedosLaden(int anzahlTorpedos) {

	}
	
	/**
	 * 
	 * @param schutzschilde
	 * <p>	
	 * Fuehrt die Reparatur der Schutzschilde durch.
	 */
	public void reparaturDurchfuehren(boolean schutzschilde) {

	}
	
	/**
	 * public void zustandRaumschiff()
	 * <p>
	 * Gibt den Zustand des Raumschiffs und das Ladungsverzeichnis wieder.
	 */
	
	public void zustandRaumschiff() {
		System.out.println("");
		System.out.println("Schiffsname: " + schiffsname);
		System.out.println("EnergieversorgungInProzent: " + energieversorgungInProzent);
		System.out.println("Ladungsverzeichnis: " + ladungsverzeichnis);
		System.out.println("SchildeInProzent: " + schildeInProzent);
		System.out.println("HuelleInProzent: " + huelleInProzent);
		System.out.println("LebenserhaltungssystemeInProzent: " + lebenserhaltungssystemeInProzent);
		System.out.println("AndroidenAnzahl: " + androidenAnzahl);
		System.out.println("Photonetorpedo Anzahl: " + photonentorpedoAnzahl);
		System.out.printf("\n\n");
	}
	/**
	 * public void ladungsverzeichnisAusgeben()
	 * <p>
	 * Dedizierte Ausgabe fuer das Ladungsverzeichnis.
	 */
	public void ladungsverzeichnisAusgeben() {

		// String List = ladungsverzeichnis.toString();
		// Ladung.get(Ladung);
		System.out.println("Ladung" + ladungsverzeichnis);
	}
	
	/**
	 * public void ladungsverzeichnisAufraeumen()
	 * <p>
	 * Raeumt das Ladungsverzeichnis auf (noch nicht genutzt).
	 */
	public void ladungsverzeichnisAufraeumen() {

	}

}
