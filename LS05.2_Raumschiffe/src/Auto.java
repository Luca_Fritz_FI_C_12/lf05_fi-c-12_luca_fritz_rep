import java.util.ArrayList;

public class Auto {
	
	private String marke;
	private String farbe;
	// private ArrayList <Ladung>ladungen newarraylist;
	
	private String besitzer;
	private int bauJahr;
	private int hubraum;
	
	public Auto() {
		System.out.println("Auto-Objekt: ohne Parameter");
	}
	public Auto(String marke, String farbe) {
		
		System.out.println("Auto-Objekt: mit Parameter");
		
		this.marke = marke;
		
		this.farbe = farbe;	
	}
	
	public void setMarke(String marke) {
		this.marke = marke;
	}
	
	public String getMarke() {
		return this.marke;
	}
	
	public void setFarbe(String farbe ) {
		this.farbe = farbe;
	}
	
	public String getFarbe() {
		return this.farbe;
	}
	
	// ende Get Set (selber gemacht)
	public int getBauJahr() {
		return bauJahr;
	}
	public void setBauJahr(int bauJahr) {
		this.bauJahr = bauJahr;
	}
	
	public String getBesitzer() {
		return besitzer;
	}
	public void setBesitzer(String besitzer) {
		this.besitzer = besitzer;
	}
	public int getHubraum() {
		return hubraum;
	}
	public void setHubraum(int hubraum) {
		this.hubraum = hubraum;
	}
	
	// weitere Methoden
	

	public void fahre(int strecke) {
		
		System.out.println("Du bist " + strecke + "km gefahren");
	}
	

	public void tanke(int liter) {
		
		System.out.println("Du hast " + liter + (" Liter getankt"));
	}
	
	
	
	

}
