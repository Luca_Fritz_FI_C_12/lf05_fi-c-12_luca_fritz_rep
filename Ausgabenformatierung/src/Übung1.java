

public class �bung1 {

	public static void main(String[] args) {
		System.out.print("Konsolenausgabe Aufgabe 1:\nS");
		System.out.println("\nAufgabe 1:\n");
		int alter = 26;
		String name = "Peter";
		
		System.out.print("Das ist " + name + " und er ist "+ alter + " alt.\n"); 
		System.out.println("Das ist der zweite \"Satz\".");
		
		//Println versetzt den String automatisch in eine neue Zeile. Print generiert alle Commands hintereinander.)
		
		System.out.print("\n Aufgabe 2:");
		System.out.println("\n      *");
		System.out.println("     ***");
		System.out.println("    *****");
		System.out.println("   *******");
		System.out.println("  *********");
		System.out.println(" ***********");
		System.out.println("*************");
		System.out.println("     ***");
		System.out.println("     ***");
		
		System.out.print("\nAufgabe 3:");
		System.out.printf("\n\n%.2f\n", 22.4234234);
		System.out.printf("%.2f\n", 111.2222);
		System.out.printf("%.2f\n", 4.0);
		System.out.printf("%.2f\n", 1000000.551);
		System.out.printf("%.2f\n", 97.34);
		
	}
	

}
